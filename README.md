# HumanChallenge
## What is this for?
This is a PHP class designed to block the action of spam bots, or internet robots (kind like captcha) employing intelligent tactics on human detection

## How can I use it?
This should be self-explanatory and simple to test. Put the files on the same directory in a test server and execute index.php script

## What are the fundamental of this?
There are a couple of aspects we can discuss about:
* In regards to design
  * This should be simple to implement on any PHP form
  * We need an easy challenge for a human but very hard for a bot
  * We don't want to rely on someone else's service for maximum availability
  * Ideally this will run on legacy infrastructure and brand new deployments as well
* The method
  * Build several different types of challenge
  * Build very random answers
  * Build very random challenge HTML structure
  * Add garbage code so bypassing attempts will be time-consuming and unreliable
* The underground factor
  * Mainstream solutions will always be targeted by abusers, chances are you will have to patch them more often