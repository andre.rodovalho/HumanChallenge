<?php
/******************************************************************

Copyright (c) 2010 Andre Campos Rodovalho

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

******************************************************************/

/**
 * This class was designed to block the action of spam bots, or internet robots (kind like captcha)
 * employing intelligent tactics on human detection
 *
 * @autor Andre Campos Rodovalho - andre.rodovalho at gmail.com
 * @date 02-08-2010
 * @update 20-06-2018
 * @version 3.6
 * @copyright 2010 Andre Campos Rodovalho
 * @license MIT
 */
class HumanChallenge{

	var $coloursNames = array( 0=>'red', 1=>'yellow', 2=>'pink', 3=>'green', 4=>'brown',
		5=>'orange', 6=>'blue', 7=>'gray' );
	var $coloursHexaCode = array( 0=>'#FF0000', 1=>'#E0E000', 2=>'#FF00FF', 3=>'#339900', 4=>'#934900',
		5=>'#FB9600', 6=>'#0000FF', 7=>'#999999' );
	var $numberNames = array( 0=>'zero', 1=>'one', 2=>'two', 3=>'three', 4=>'four', 5=>'five',
		6=>'six', 7=>'seven', 8=>'eight', 9=>'nine' );

	
	/**
	 * Creates an instance of the class and outputs a challenge if requested
	 * @param boolean $startChallenge Flag to output challenge
	 * @param boolean $randomChallenge Flag to make challenge random
	 * @param int $challengeToUse When $randomChallenge is false, calls the corresponding method
	 */
	function HumanChallenge($startChallenge=true,$randomChallenge=true,$challengeToUse=0){
		@session_start();

		if( $randomChallenge )
			$challengeToUse = rand(0,3);

		if( $startChallenge ){
			switch( $challengeToUse ){
				case 0: echo $this->buildMathChallenge(); break;
				case 1: echo $this->buildCountingChallenge(); break;
				case 2: echo $this->buildColourChallenge(); break;
				case 3: echo $this->buildBackwardsChallenge(); break;
			}
		}
	}

	/**
	 * Generates a random arithmetic challenge
	 * @param string $storeAnswer Flag to store or not the answer generated
	 * @return string $challenge
	 */
	function buildMathChallenge($storeAnswer=true){

		$operationsString = array(0=>'plus',1=>'minus',2=>'fold');

		$indexNumber1 = rand(0,9);
		$indexNumber2 = rand(0,9);
		$indexOperation = rand(0,2);

		$answer = 0;
		switch($indexOperation){
		    case 0: $answer = $indexNumber1 + $indexNumber2; break;
		    case 1: $answer = $indexNumber1 - $indexNumber2; break;
		    case 2: $answer = $indexNumber1 * $indexNumber2; break;
		}
		if( $storeAnswer ) $this->storeAnswer($answer);

		$garbage0 = $garbage1 = $garbage2 = '';
		$variable = 'garbage'.rand(0,2);
		$$variable = $this->buildGarbage();

		$challenge = 'Please answer '.$garbage2.'with digits the result'.$garbage0.
			' of the operation "'.$this->numberNames[$indexNumber1].' '.
			$operationsString[$indexOperation].' '.$this->numberNames[$indexNumber2].
			'" using - if negative'.$garbage1;

		$pos0 = $pos1 = '';
		if( $storeAnswer ) { // avoiding recursive calls
			$position = 'pos'.rand(0,1);
			$$position = '<span><div style="display:'.$this->buildSpacer().'none">'.
				$this->buildCountingChallenge(false).'</div></span>';
		}

		return $pos1.'<span><div style="display:'.$this->buildSpacer().'none">'.time().'</div>'.
			$challenge.'</span>'.$pos0;
	}

	/**
	 * Stores answer in SESSION
	 * @param string $answer Answer to be stored
	 * @return void
	 */
	function storeAnswer($answer){
		$_SESSION['antibotanswer'] = $answer;
	}

	/**
	 * Checks if user answer match stored answer
	 * @param string $answer Sent answer (user input)
	 * @return boolean
	 */
	function checkAnswer($answer){
		if( ($_SESSION['antibotanswer'] == $answer) && !empty($_SESSION['antibotanswer']) ) return true;
		else{ $_SESSION['antibotanswer'] = ''; return false; };
	}

	/**
	 * Generates a random challenge on counting how many times a character occurs in a string
	 * @param string $storeAnswer Flag to store or not the answer generated
	 * @return string $challenge
	 */
	function buildCountingChallenge($storeAnswer=true){

		$word_size = 9;
		$word = $this->buildWord($word_size);

		$indexPicked = rand(1,$word_size);

		$charPicked = substr( $word, $indexPicked, 1 );
		$answer = substr_count( $word, $charPicked );
		if( $storeAnswer ) $this->storeAnswer($answer);

		$garbage0 = $garbage1 = $garbage2 = '';
		$variable = 'garbage'.rand(0,2);
		$$variable = $this->buildGarbage(); // should override null value

		$challenge = 'Answering with digits, how many times '.$garbage0.'the character '
			.$garbage1.'"'.$charPicked.'" occurs on: '.$garbage2.'<b>'.$word.'</b> ?';

		$pos0 = $pos1 = '';
		if( $storeAnswer ) {
			$position = 'pos'.rand(0,1);
			$$position = '<span><div style="display:'.$this->buildSpacer().'none">'.
				$this->buildMathChallenge(false).'</div></span>';
		}

		return $pos1.'<span><div style="display:'.$this->buildSpacer().'none">'.time().'</div>'.
			$challenge.'</span>'.$pos0;
	}

	/**
	 * Generates a word (main)
	 * @param int $word_size Desired lenght
	 * @return string $word
	 */
    	function buildWord($word_size=5){
		$vowels = array('a','e','i','o','u');
		$consonants = array('b','c','d','f','g','h','nh','lh','ch','j','k','l','m','n','p','qu',
			'r','rr','s','ss','t','v','w','x','y','z');

		$word = '';
		$syllables_counter = 0;
		while($syllables_counter < $word_size){

			$vowel = $vowels[rand(0,count($vowels)-1)];
			$consonant = $consonants[rand(0,count($consonants)-1)];
			$syllable = $consonant.$vowel;
			$word .=$syllable;
			$syllables_counter++;
			unset($vowel,$consonant,$syllable);
		}

		return $word;
	}

 	/**
	 * Generates random garbage html to hopefully mislead bots
	 * @return string $garbage
	 */
	function buildGarbage(){

		$openTag = array('<p style="display:'.$this->buildSpacer().'none">',
			'<span style="display:'.$this->buildSpacer().'none">',
			'<tt style="display:'.$this->buildSpacer().'none">',
			'<font style="display:'.$this->buildSpacer().'none">',
			'<div style="display:'.$this->buildSpacer().'none">',
			'<cite style="display:'.$this->buildSpacer().'none">');

		$closeTag = array('</p>','</span>','</tt>','</font>','</div>','</cite>');

		$indexTag = rand(0,5);

		if( rand(0,1) )
			$garbage = time();
		else
			$garbage = $this->buildWord( rand(4,11) );

		$garbage = $openTag[$indexTag].$garbage.$closeTag[$indexTag];
		if( rand(0,1) ) $garbage = str_replace('">', ';">', $garbage);

		return $garbage;
	}

	/**
	 * Generates string with random amount space characters
	 * @return string $spacer
	 */
	function buildSpacer(){
		$spacer = '';
		for( $i=0; $i< rand(0,5); $i++ ){
			$spacer .= ' ';
		}

		return $spacer;
	}


	/**
	 * Generates a challenge on typing chars having a specific colour from a string
	 * @param string $storeAnswer Flag to store or not the answer generated
	 * @return string $challenge
	 */
	function buildColourChallenge($storeAnswer=true){

		$str = array();
		$srt_size = 15;
		$coloured_amount = rand(1,5); //answer
		$answer = array();
		$coloured_index = rand(0,7); //picked
		$other_colours = array();
		while(1){ //pick three different cores
			if( sizeof($other_colours) > 2 )
				break;
			else{
				$index = rand(0,7);
				if( $index != $coloured_index )
					$other_colours[] = $this->coloursHexaCode[$index];
			}
		}

		$entry = $this->buildEntry($srt_size);
		$entry = str_split($entry);

		$y = 0; // filling chars counter
		for( $i = 0; $i<$srt_size; $i++ ) { //building the core of this challenge

			$char = $entry[$i];

			// check if we should add picked colour
			if( ($y < $coloured_amount) && ($coloured_amount != 0) ){
				$colour = $this->coloursHexaCode[$coloured_index];
				$answer[$char.$i] = $char;
				$y++;
			}else{ // pick a garbage colour 
				$index = rand(0,2);
				$colour = $other_colours[$index];
			}

			$str[$char.$i] = '<font color="'.$colour.'">'.$char.'</font> ';
		}

		ksort($answer);
		ksort($str);
		$str = implode($str);
		$answer = implode($answer);
		if( $storeAnswer ) $this->storeAnswer($answer);

		$garbage0 = $garbage1 = $garbage2 = '';
		$variable = 'garbage'.rand(0,2);

		$challenge = 'Your answer is the characters in '.$this->coloursNames[$coloured_index].
			' considering uppercase / lowercase'.$garbage0.' of the following string '.
			' <br />( <strong>'.$str.$garbage2.'</strong> )'.$garbage1;

		$pos0 = $pos1 = '';
		if( $storeAnswer ) {
			$position = 'pos'.rand(0,1);
			$$position = '<span><div style="display:'.$this->buildSpacer().'none">'.
				$this->buildBackwardsChallenge(false).'</div></span>'; // should override null value
		}

		return $pos1.'<span><div style="display:'.$this->buildSpacer().'none">'.time().'</div>'.
			$challenge.'</span>'.$pos0;
	}

	/**
	 * Generates an entry (main)
	 * @param int $srt_size Desired lenght
	 * @return string $entry
	 */
	function buildEntry($srt_size=5){

		$entry = '';
		srand( (double)microtime()*1000000 );
		$data = "AbcDE123IJKLMN67QRSTUVWXYZ";
		$data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
		$data .= "FGH45P89";

		for( $i = 0; $i<$srt_size; $i++ ) {
			$entry .= substr($data, (rand()%(strlen($data))), 1);
		}

		return $entry;
	}
	
	/**
	 * Generates a challenge on typing chars without an determined number, inverting the string or not
	 * @return string $challenge
	 */
	function buildBackwardsChallenge($storeAnswer=true){

		$chars = $this->buildEntry(5);
		$chars = str_split($chars);
		$removeThis = 0;

		foreach($chars as $char){ // searches for a number to remove from string
			if( intval($char) != 0 ){ // number found!
				$removeThis = $char;
				break;
			}
		}
		if( $removeThis == 0 ){ // if no numbers found, add one
			$removeThis = rand(1,9); // do not add 0, avoiding confusion with 0 / O
			$chars[] = $removeThis;
		}

		$chars = implode($chars);
		$answer = str_replace($removeThis,'',$chars);
		$invert = '';
		if( rand(0,1) ) {
			$answer = strrev( $answer );
			$invert = '<strong>backwards</strong>';
		}
		if( $storeAnswer ) $this->storeAnswer($answer);

		$garbage0 = $garbage1 = $garbage2 = '';
		$variable = 'garbage'.rand(0,2);
		$$variable = $this->buildGarbage(); // should override null value

		$challenge = 'Please type <font color="'.$this->coloursHexaCode[rand(0,7)].'">'.
			$chars.'</font> '.$invert.' ignoring'.$garbage1.' the number
			<strong>'.$this->numberNames[$removeThis].'</strong>'.$garbage0.
			' for your answer. Consider lowercase '.$garbage2.'/ uppercase characters';

		$pos0 = $pos1 = '';
		if( $storeAnswer ) {
			$position = 'pos'.rand(0,1);
			$$position = '<span><div style="display:'.$this->buildSpacer().'none">'.
				$this->buildColourChallenge(false).'</div></span>';
		}

		return $pos1.'<span><div style="display:'.$this->buildSpacer().'none">'.time().'</div>'.
			$challenge.'</span>'.$pos0;
	}

}
